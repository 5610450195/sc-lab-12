package modelPhoneBook;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Data {
	private String name;
	private String phone;
	
	public Data(String name, String phone){
		this.name = name;
		this.phone = phone;
	}
	
	public void writeBook(){
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter("phonebook.txt", true);
			PrintWriter out = new PrintWriter(fileWriter);
			out.println(name + ", " + phone);
			out.flush();
	 	}
	 	catch (IOException e){
	 		System.err.println("Error reading from user");
	 	}
		finally{
			if(fileWriter != null)
				try {
					fileWriter.close();
				} 
				catch (IOException e) {
					System.err.println("Error closing files");
				}
		}
	}

}
