package modelPhoneBook;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class PhoneBook {
	
	public void readPhoneBook() {
		String filename = "phonebook.txt";
		FileReader fileReader = null;
		try {
			 fileReader = new FileReader(filename);
			 BufferedReader buf = new BufferedReader(fileReader);
			 System.out.println("--------------- Java Phone Book ---------------\n");
			 String line = buf.readLine();
			 while (line != null) {
				 line.split(",");
				 System.out.print("\t>>\tName : "+line.substring(0,line.indexOf(",")).toUpperCase()+"\n");
				 System.out.println("\t>>\tTel  : "+line.substring(line.indexOf(",")+1).trim()+"\n");
				 line = buf.readLine();
			 }
	 	}
		catch (FileNotFoundException e) {
			 System.err.println("Cannot read file "+filename);
	 	}
		
	 	catch (IOException e) {
			 System.err.println("Error reading from file");
	 	}
		
		finally {
			if (fileReader != null)
				try {
					fileReader.close();
				} 
				catch (IOException e) {
					System.err.println("Error closing files");
				}
		}
	}
}
