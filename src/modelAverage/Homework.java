package modelAverage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class Homework {
	public void average(String filename) {
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line = buffer.readLine();
			
			FileWriter fileWriter = new FileWriter("average.txt", true);
			PrintWriter out = new PrintWriter(fileWriter);
			
			if (filename.equalsIgnoreCase("homework.txt")) {
				out.println("========== Homework Scores ==========");
			}
			else if(filename.equalsIgnoreCase("exam.txt")) {
				out.println("========== Exam Scores ==========");
			}
			out.println("Name\tAverage\n----\t----");
			
			while (line != null) {
				 double sum = 0;
				 int num = line.length()-line.replace(",", "").length();
				 String name = line.substring(0,line.indexOf(","));
				 line = line.substring(line.indexOf(",")+1).trim();
				 for (int i=0 ; i<num ; i++) {
					 if (line.indexOf(",") == -1) {
						 sum += Double.parseDouble(line.trim());
					 }
					 else {
						 sum += Double.parseDouble(line.substring(0,line.indexOf(",")));
						 line = line.substring(line.indexOf(",")+1).trim();
					 }
				 }
				 out.println(name.toUpperCase()+"\t"+sum/num);
				 out.flush();
				 line = buffer.readLine();
			 }	
	 	 }
	 	 catch (IOException e) {
			 System.err.println("Error reading from file");
	 	 }
		
		finally {
			if (fileReader != null)
				try {
					fileReader.close();
				} 
				catch (IOException e) {
					System.err.println("Error closing files");
				}
		}
	}
	public void showInAverage() {
		FileReader fileReader = null;
		try {
			fileReader = new FileReader("average.txt");
			BufferedReader buf = new BufferedReader(fileReader);
			String line = buf.readLine();
			while (line != null) {
				System.out.println(line);
				line = buf.readLine();
			}
		}
		
		catch (IOException e) {
			System.err.println("Error reading from user");
		}
		
		finally {
			if (fileReader != null)
				try {
					fileReader.close();
				} 
				catch (IOException e) {
					System.err.println("Error closing files");
				}
		}
	}
}
