package modelAverage;

public class HomeworkMain {
	public static void main(String[] args) {
		Homework n = new Homework();
		n.average("homework.txt");
		n.showInAverage();
		System.out.println("\n");
		
		n.average("homework.txt");
		n.average("exam.txt");
		n.showInAverage();
	}
}
